# birdfeed
Look for new entries in an RSS feed and post them to a Twitter account

## Operation ##

1. Load the RSS feed and loop through entries
2. Check to see if the entry has been posted to Twitter
3. Post the entry to Twitter and log it

## Load the RSS Feed ##

Use [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/) for this.

## Post the Entry to Twitter ##

Use [TwitterAPI](https://github.com/geduldig/TwitterAPI) or [Twython](https://github.com/ryanmcgrath/twython).

1. Load Twitter auth keys
2. Grab title and URL
    3. Do I care if it's a link post or not?
    4. How can I track that?
3. Construct the Twitter post and post it

## Log It ##

sqlite?
key/value store

